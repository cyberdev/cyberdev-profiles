# Depends on these modules currently:
# - saz-ssh
# - puppetlabs-firewall
#
class profiles::base {

  ## Hiera lookups
  $site_name               = hiera('profiles::site::site_name')
  $sensor_name             = hiera('profiles::sensor::sensor_name')
  
  # trusted_net defaults to the local management network, as determined by facter
  # All services, will by default accept connections from this network. Further refinement
  # can be performed using the service specific values below.
  $trusted_net             = hiera('profiles::sensor::trusted_net',  "$::ipaddress/$::netmask")
  $trusted_ssh             = hiera('profiles::sensor::trusted_ssh',  "$trusted_net")
  $trusted_http            = hiera('profiles::sensor::trusted_http', "$trusted_net")

  # Include R.O.C.K. base firewall rules
  resources { "firewall": purge => true }
  Firewall { before => Class['rock_fw::post'], require => Class['rock_fw::pre'],}
  class { ['rock_fw::pre', 'rock_fw::post']:}
  class { 'firewall': }

  # Enable SSH connectivity
  class { 'ssh::server':
    storeconfigs_enabled => false,
    options => {
      'PermitRootLogin'        => 'no',
      'Port'                   => '22',
    }
  }
  firewall { '100 allow ssh access' :
    port   => 22,
    proto  => tcp,
    source => $trusted_ssh,
    action => accept,
  }

  # Enable nginx for proxy
  # TODO: move this to another class that can be included in the classes that need it
  class { 'nginx': }
  firewall { '150 allow http and https access':
    port   => [80, 443],
    proto  => tcp,
    source => $trusted_http,
    action => accept,
  }
}

