# Depends on these modules currently:
# - https://github.com/naturalis/puppet-suricata
#
class profiles::suricata {

  ## Hiera lookups
  $site_name               = hiera('profiles::site::site_name')
  $sensor_name             = hiera('profiles::sensor::sensor_name')
  $monitor_if              = hiera('profiles::sensor::monitor_if')

  class { '::suricata':
    monitor_interface => $monitor_if,
  }

}


